# Anime Quotes
> https://quotes.kody.workers.dev/

## How to add quotes
- Add a new entry to the array in [`workers-site/quote.js`](/workers-site/quote.js)  
  Please use right apostrophes (`’`) instead of a simple single quote (`'`) in your text
    ```js
    {
      text: 'Your quote here',
      character: 'The name of the character',
      imgName: 'some-image.png'
    },
    ```
- If needed, put your character picture in [`sourceImages`](/sourceImages)
    - File has to be a transparent PNG, preferably it doesn't cut on any sides except the bottom
    - One of the two dimensions should be at least 512px long
    - The CI will run a script to make different sizes and compress the images automatically

## How to dev
- You'll need a Cloudflare account, the free tier is good enough
- Git clone this repo
- [Get Cloudflare's Wrangler working](https://developers.cloudflare.com/workers/)
    - i.e. install with `npm i -g wrangler`
    - And then login with `wrangler login`
- Copy `wrangler.toml.dist` to `wrangler.toml` and fill your `account_id`
    - You can get find it with `wrangler whoami`
- Run `wrangler dev`, it will deploy the code on changes to a dev environment and open a tunnel to access it
- Then you can try publishing the code to your account with `wrangler publish`

## API
The `/random` route returns a random quote, pretty straightforward.  
Here's an example. There's no `null` values, everything's a string except `id`.

`GET https://quotes.kody.workers.dev/random`
```json
{
  "text": "Remember the lesson, not the disappointment.",
  "character": "Holo The Wise Wolf",
  "imgName": "Holo.png",
  "img": "https://quotes.kody.workers.dev/img/Holo.png",
  "imgSizes": {
    "small": "https://quotes.kody.workers.dev/img/Holo-small.png",
    "medium": "https://quotes.kody.workers.dev/img/Holo-medium.png",
    "large": "https://quotes.kody.workers.dev/img/Holo-large.png"
  },
  "id": 7
}
```

If you have an ID, you can request that quote. Note that it starts at 0.

`GET https://quotes.kody.workers.dev/get?id=6`
```json
{
  "text": "It’s impossible to work hard for something you don’t enjoy.",
  "character": "Silica",
  "imgName": "Silica.png",
  "img": "https://quotes.kody.workers.dev/img/Silica.png",
  "imgSizes": {
    "small": "https://quotes.kody.workers.dev/img/Silica-small.png",
    "medium": "https://quotes.kody.workers.dev/img/Silica-medium.png",
    "large": "https://quotes.kody.workers.dev/img/Silica-large.png"
  },
  "id": 6
}
```

Calling `/get`...
- without the `id` parameter will return a 422  
- with an `id` that isn't a number will return a 400  
- with an unknown `id` will return a 404
