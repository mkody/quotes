const quotes = [
  {
    text: 'Knowing you’re different is only the beginning. If you accept these differences you’ll be able to get past them and grow even closer.',
    character: 'Miss Kobayashi',
    imgName: 'Kobayashi.png'
  },
  {
    text: 'Life is not a game of luck. If you wanna win, work hard.',
    character: 'Sora',
    imgName: 'SoraNGNL.png'
  },
  {
    text: 'If you just submit yourself to fate, then that’s the end of it.',
    character: 'Keiichi Maebara',
    imgName: 'KeiichiMaebara.png'
  },
  {
    text: 'Sometimes I do feel like I’m a failure. Like there’s no hope for me. But even so, I’m not gonna give up. Ever!',
    character: 'Izuku Midoriya',
    imgName: 'Deku.png'
  },
  {
    text: 'Life is like a tube of toothpaste. When you’ve used all the toothpaste down to the last squeeze, that’s when you’ve really lived. Live with all your might, and struggle as long as you have life.',
    character: 'Mion Sonozaki',
    imgName: 'SonozakiMion.png'
  },
  {
    text: 'An excellent leader must be passionate because it’s their duty to keep everyone moving forward.',
    character: 'Nico Yazawa',
    imgName: 'NicoYazawa.png'
  },
  {
    text: 'It’s impossible to work hard for something you don’t enjoy.',
    character: 'Silica',
    imgName: 'Silica.png'
  },
  {
    text: 'Remember the lesson, not the disappointment.',
    character: 'Holo The Wise Wolf',
    imgName: 'Holo.png'
  },
  {
    text: 'Love is simply an electrical bug in the human neural circuit.',
    character: 'Akasaka Ryuunosuke',
    imgName: 'Ryuunosuke.png'
  },
  {
    text: 'It’s a programmer’s job to make the most of limited resources to turn an impractical idea into reality.',
    character: 'Akasaka Ryuunosuke',
    imgName: 'Ryuunosuke.png'
  },
  {
    text: 'Fear is freedom! Subjugation is liberation! Contradiction is truth! Those are the facts of this world! And you will all surrender to them, you pigs in human clothing!',
    character: 'Satsuki Kiryuuin',
    imgName: 'Satsuki.png'
  },
  {
    text: 'Mistaking lust for love shows that you’re caught in an inflationary spiral of sexual frustration.',
    character: 'Tsukihi Araragi',
    imgName: 'Tsukihi.png'
  },
  {
    text: 'Back then, if we could have have heard each other’s voices, everything would have been so much better.',
    character: 'Shouya Ishida',
    imgName: 'Ishida.png'
  },
  {
    text: 'I yearn for true gender equality.',
    character: 'Kazuma Satou',
    imgName: 'Kazuma.png'
  },
  {
    text: 'Explosion!!!',
    character: 'Megumin',
    imgName: 'Megumin.png'
  },
  {
    text: 'The only one who can decide your worth... is you. If you want to earn something, you need to reach out for it.',
    character: 'Jabami Yumeko',
    imgName: 'JabamiYumeko.png'
  },
  {
    text: 'Deceive your other self. Deceive the world. That is what you must do to reach the Steins Gate. Good Luck. El Psy Congroo.',
    character: 'Okabe Rintaro',
    imgName: 'Rintaro.png'
  },
  {
    text: 'If you want to grant your own wish, then you should clear your own path to it.',
    character: 'Okabe Rintaro',
    imgName: 'Rintaro.png'
  },
  {
    text: 'No one knows what the future holds. That’s why its potential is infinite.',
    character: 'Okabe Rintaro',
    imgName: 'Rintaro.png'
  },
  {
    text: 'Even if the world line changes, as long as you don’t forget me, I’m there.',
    character: 'Kurisu Makise',
    imgName: 'Kurisu.png'
  },
  {
    text: 'Don’t believe in yourself. Believe in ME who believes in you!',
    character: 'Kamina',
    imgName: 'Kamina.png'
  },
  {
    text: 'Having someone to lean on is an amazing thing. Some things that can’t be done alone can be achieved with the courage of two. And in times when you try your hardest and still can’t make it work. When it’s so tough you’re ready to cry... if you have someone there to support you, you can make it through. The smallest words of support can save you.',
    character: 'Gokou Ruri',
    imgName: 'RuriGokou.png'
  },
  {
    text: 'We’re always serious. We’re seriously joking around.',
    character: 'Anarchy Stocking',
    imgName: 'Stocking.png'
  },
  {
    text: 'You don’t have to worry about being special or normal compared to everyone else! It doesn’t matter who it is. So as long as they think you’re special, then you’re special.',
    character: 'Eru Chitanda',
    imgName: 'Chitanda.png'
  },
  {
    text: 'The scars that you can’t see are the hardest to heal.',
    character: 'Nao Tomori',
    imgName: 'NaoTomori.png'
  }
]

function getQuote (id, url) {
  if (!id) id = Math.floor(Math.random() * quotes.length)

  const quote = quotes[id]
  if (!quote) return null

  quote.img = `https://${url.host}/img/${quote.imgName}`
  quote.imgSizes = {
    small: `https://${url.host}/img/${quote.imgName.replace('.png', '-small.png')}`,
    medium: `https://${url.host}/img/${quote.imgName.replace('.png', '-medium.png')}`,
    large: `https://${url.host}/img/${quote.imgName.replace('.png', '-large.png')}`
  }
  quote.id = id

  return quote
}

export { getQuote }
