/* global addEventListener, Response, Request */
import { getAssetFromKV } from '@cloudflare/kv-asset-handler'
import { getQuote } from './quote.js'

/**
 * The DEBUG flag will do two things that help during development:
 * 1. we will skip caching on the edge, which makes it easier to
 *    debug.
 * 2. we will return an error message on exception in your Response rather
 *    than the default 404.html page.
 */
const DEBUG = false

addEventListener('fetch', event => {
  event.respondWith(handleEvent(event))
})

async function handleEvent (event) {
  const options = {}

  try {
    if (DEBUG) {
      // customize caching
      options.cacheControl = {
        bypassCache: true
      }
    }

    let response

    const url = new URL(event.request.url)
    if (url.pathname === '/random') {
      const quote = getQuote(null, url)

      response = new Response(JSON.stringify(quote))
      response.headers.set('Content-Type', 'application/json')
      response.headers.set('Access-Control-Allow-Origin', '*')
    } else if (url.pathname === '/get') {
      const id = url.searchParams.get('id')

      if (!id) {
        return new Response('Missing `id` parameter.', { status: 422 })
      } else if (isNaN(id)) {
        return new Response('`id` is not a number.', { status: 400 })
      } else {
        const quote = getQuote(id, url)

        if (!quote) {
          return new Response('Quote not found.', { status: 404 })
        } else {
          response = new Response(JSON.stringify(quote))
          response.headers.set('Content-Type', 'application/json')
          response.headers.set('Access-Control-Allow-Origin', '*')
        }
      }
    } else {
      const page = await getAssetFromKV(event, options)
      response = new Response(page.body, page)

      response.headers.set('X-XSS-Protection', '1; mode=block')
      response.headers.set('X-Content-Type-Options', 'nosniff')
      response.headers.set('X-Frame-Options', 'DENY')
      response.headers.set('Referrer-Policy', 'unsafe-url')
      response.headers.set('Feature-Policy', 'none')

      if (url.pathname.startsWith('/img')) {
        response.headers.set('Cache-Control', 'public, maxage=' + (30 * 24 * 60 * 60))
        response.headers.append('Cache-Control', 's-maxage=' + (30 * 24 * 60 * 60))
      }
    }

    return response
  } catch (e) {
    // if an error is thrown try to serve the asset at 404.html
    if (!DEBUG) {
      try {
        const notFoundResponse = await getAssetFromKV(event, {
          mapRequestToAsset: req => new Request(`${new URL(req.url).origin}/404.html`, req)
        })

        return new Response(notFoundResponse.body, { ...notFoundResponse, status: 404 })
      } catch (e) {}
    }

    return new Response(e.message || e.toString(), { status: 500 })
  }
}
