/*
  The goal of this script is to resize images into three other sizes
  and compress everything.
*/

import fs from 'fs'
import ora, { oraPromise } from 'ora'
import glob from 'glob'
import jimp from 'jimp'
import imagemin from 'imagemin'
import imageminPngquant from 'imagemin-pngquant'

const srcDir = '../sourceImages/' // With trailing slash
const destDir = '../public/img/'

// Function to resize
function resize (file, suffix, size) {
  const basename = file.replace(srcDir, '').slice(0, -4)
  const destination = './tmp/' + basename + '-' + suffix + '.png'

  return jimp.read(file)
    .then(image => {
      return image
        .scaleToFit(size, size)
        .write(destination)
    })
    .catch(console.error)
}

// Create a temp folder to work
fs.mkdir('./tmp', { recursive: true }, err => {
  if (err) throw err

  // List all of our source PNGs
  glob(srcDir + '*.png', async (err, files) => {
    if (err) throw err

    const length = files.length
    let prorgress = 0
    let spinner = ora().start('Resizing images')

    // Resize our PNGs into 3 sizes
    for (const file of files) {
      spinner.text = `Resizing images ${++prorgress}/${length}`
      await resize(file, 'small', 128)
      await resize(file, 'medium', 256)
      await resize(file, 'large', 512)
    }

    spinner = spinner.succeed('Images resized')

    // Compress images
    await oraPromise(
      imagemin(
        [
          './tmp/*.png',
          srcDir + '*.png'
        ],
        {
          destination: destDir,
          plugins: [
            imageminPngquant({
              quality: [0.6, 0.8]
            })
          ]
        }
      ),
      {
        text: 'Compressing images',
        successText: 'Images compressed'
      }
    )

    // Cleanup
    fs.rm('./tmp', { recursive: true }, err => {
      if (err) throw err

      spinner.succeed('Done!')
    })
  })
})
